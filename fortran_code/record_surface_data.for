      subroutine record_surface_data(dim,data_dir,mtlidx_file,grid_file_surface,obj_file,mtllib_file,surface_properties_file,
     &     Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record the surface data binary files
c     
c     Input:
c       + dim: dimension of space
c       + data_dir: base directory for finding input data files
c       + mtlidx_file: file that will record the index of each declared material
c       + grid_file_surface: file that will record geometric data of the surface
c       + obj_file: intermediary OBJ file produced at run time
c       + mtllib_file: name of the materials library file
c       + surface_properties_file: file that will record material and temperature information for the surface
c       + Nv: total number of vertices
c       + Nf: total number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + Nmat: number of materials
c       + materials: array of materials (name of the material for each face)
c       + mtl_idx: index of the material (in the "materials" list) for each face
c     
c     Ouput:
c       + the required binary files & materials list file
c     
c     I/O
      integer dim
      character*(Nchar_mx) data_dir
      character*(Nchar_mx) mtlidx_file
      character*(Nchar_mx) grid_file_surface
      character*(Nchar_mx) obj_file
      character*(Nchar_mx) mtllib_file
      character*(Nchar_mx) surface_properties_file
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      integer mtl_idx(1:Nf_mx)
c     temp
      character*(Nchar_mx) temperature_file
      integer Nt
      double precision surf_T(1:Nt_mx,1:2)
      logical invert_normal
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_data'

      write(*,*) 'Recording output files...'
c     read the latitudinal dependance of surface temperature
      temperature_file=trim(data_dir)//'surface_temperature.dat'
      call read_surface_temperature_file(temperature_file,Nt,surf_T)
c     -------------------------------------------------------------------------------
c     List of materials
      call record_surface_materials_list(Nmat,materials,mtlidx_file)
c     -------------------------------------------------------------------------------
c     Surface grid file
      call record_surface_grid_file(dim,Nv,Nf,vertices,faces,grid_file_surface)
c     -------------------------------------------------------------------------------
c     Surface properties file
      call record_surface_properties_file(dim,Nv,Nf,vertices,faces,mtl_idx,Nt,surf_T,surface_properties_file)
c     -------------------------------------------------------------------------------
c     Additionnaly, produce the standard OBJ file
      invert_normal=.false.
      call write_obj(dim,obj_file,mtllib_file,invert_normal,
     &     Nv,Nf,vertices,faces,Nmat,materials,mtl_idx)
      write(*,*) 'File has been recorded: ',trim(obj_file)

      return
      end



      subroutine record_surface_properties_file(dim,Nv,Nf,vertices,faces,mtl_idx,Nt,surf_T,surface_properties_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c     
c     Purpose: to record a surface properties binary file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: coordinates of each vertex
c       + faces: indexes of the vertices for each face
c       + mtl_idx: index of the material (in the "materials" list) for each face
c       + Nt: number of theta/T values
c       + surf_T: values of the latitude [deg] and associated surface temperature [K]
c       + surface_properties_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer mtl_idx(1:Nf_mx)
      integer Nt
      double precision surf_T(1:Nt_mx,1:2)
      character*(Nchar_mx) surface_properties_file
c     temp
      integer*8 Nv8
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer*8 record_size,alignment
      double precision xG(1:Ndim_mx)
      double precision r,theta,phi,T
      integer iface,i
c     Debug
c$$$      integer mtlidx_min,mtlidx_max
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_properties_file'

      record_size=int(size_of_int4+size_of_real,kind(record_size))
      alignment=record_size

c     Debug
c$$$      mtlidx_min=Nmaterial_mx
c$$$      mtlidx_max=0
c$$$      do iface=1,Nf
c$$$         if (mtl_idx(iface).lt.mtlidx_min) then
c$$$            mtlidx_min=mtl_idx(iface)
c$$$         endif
c$$$         if (mtl_idx(iface).gt.mtlidx_max) then
c$$$            mtlidx_max=mtl_idx(iface)
c$$$         endif
c$$$      enddo                     ! iface
c$$$      write(*,*) 'mtlidx_min=',mtlidx_min
c$$$      write(*,*) 'mtlidx_max=',mtlidx_max
c     Debug
      
      open(23,file=trim(surface_properties_file),form='unformatted',access='stream')
      write(23) int(pagesize,kind(Nv8))
      write(23) int(Nf,kind(Nv8))
      write(23) record_size
      write(23) alignment
c     Padding file 23 ---
      total_recorded=4*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      do iface=1,Nf
c     Compute the coordinates of the gravity center for the current face
         call face_gravity_center(dim,Nv,Nf,vertices,faces,iface,xG)
c     evaluate temperature at gravity center... all we need is its longitude
         call cart2spher(xG(1),xG(2),xG(3),r,theta,phi) ! theta & phi: [rad]
         theta=theta*180.0D+0/pi ! rad -> deg
c     and finally, linearly interpolate surface temperature from data points
         call get_surface_temperature(Nt,surf_T,theta,T)
c     Record information for the current face !
         write(23) mtl_idx(iface)-1,real(T) ! material indexes should start @ 0
      enddo                     ! j
c     Padding file 23 ---
      total_recorded=Nf*record_size
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      close(23)
      write(*,*) 'File has been recorded: ',trim(surface_properties_file)

      return
      end



      subroutine record_surface_grid_file(dim,Nv,Nf,vertices,faces,grid_file_surface)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
c     
c     Purpose: to record a surface grid file
c     
c     Input:
c       + dim: dimension of space
c       + Nv: number of vertices
c       + Nf: number of faces
c       + vertices: coordinates of each vertex
c       + faces: indexes of the vertices for each face
c       + grid_file_surface: file to record
c     
c     Output: the required binary grid file
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      character*(Nchar_mx) grid_file_surface
c     temp
      integer*8 Nv8
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer i,j,ivertex,iface
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_grid_file'

      open(22,file=trim(grid_file_surface),form='unformatted',access='stream')
      write(22) int(pagesize,kind(Nv8))
      write(22) int(Nv,kind(Nv8))
      write(22) int(Nf,kind(Nv8))
      write(22) dim
      write(22) Nvinface
c     Padding file 22 ---
      total_recorded=3*size_of_int8+2*size_of_int4
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      do ivertex=1,Nv
         write(22) (vertices(ivertex,j),j=1,dim)
      enddo                     ! iv
c     Padding file 22 ---
      total_recorded=dim*Nv*size_of_double
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      do iface=1,Nf
         write(22) (int(faces(iface,j),kind(Nv8))-1,j=1,Nvinface)
      enddo                     ! j
c     Padding file 22 ---
      total_recorded=Nvinface*Nf*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(22) (l1,i=1,remaining_byte)
c     --- Padding file 22
      close(22)
      write(*,*) 'File has been recorded: ',trim(grid_file_surface)

      return
      end
      

      
      subroutine record_surface_materials_list(Nmat,materials,mtlidx_file)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record the list of materials definition files
c     
c     Input:
c       + Nmat: number of materials
c       + materials: material definition file for each material
c       + mtlidx_file: materials list file to record
c     
c     Output: the required (ascii) file
c     
c     I/O
      integer Nmat
      character*(Nchar_mx) materials(1:Nmaterial_mx)
      character*(Nchar_mx) mtlidx_file
c     temp
      integer imat
c     label
      character*(Nchar_mx) label
      label='subroutine record_surface_materials_list'

      open(21,file=trim(mtlidx_file))
      write(21,*) Nmat
      do imat=1,Nmat
         write(21,10) trim(materials(imat))
      enddo                     ! imat
      close(21)
      write(*,*) 'File has been recorded: ',trim(mtlidx_file)

      return
      end


      
      subroutine face_gravity_center(dim,Nv,Nf,vertices,faces,iface,xG)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the center of gravity for a given face
c     
c     Input:
c       + dim: dimension of space
c       + Nv: total number of vertices
c       + Nf: total number of faces
c       + vertices: array of verices definition (3D position of each vertex)
c       + faces: array of faces (index of vertices that belong to each face)
c       + iface: index of the face the center of gravity has to be computed for
c     
c     Output:
c       + xG: coordinates of the center of gravity for face index "iface"
c     
c     I/O
      integer dim
      integer Nv
      integer Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer iface
      double precision xG(1:Ndim_mx)
c     temp
      integer iv,j
      double precision sum_x(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine face_gravity_center'

c     Compute the coordinates of the gravity center for the current face
      do iv=1,dim
         sum_x(iv)=0.0D+0
         do j=1,3
            sum_x(iv)=sum_x(iv)+vertices(faces(iface,iv),j)
         enddo                  ! j
         xG(iv)=sum_x(iv)/3.0D+0 ! this is the Gravity center
      enddo                     ! iv
      
      return
      end
      


      subroutine get_surface_temperature(Nt,surf_T,theta,T)
      implicit none
      include 'max.inc'
c     
c     Purpose: to interpolate the value of the surface temperature at a given latitude
c     
c     Input:
c       + Nt: number of reference temperature points
c       + surf_T: reference surface temperature array (values of latitude [deg] and surface temperature [K])
c       + theta: latitude [deg]
c     
c     Output:
c       + T: surface temperature @ theta [K]
c     
c     I/O
      integer Nt
      double precision surf_T(1:Nt_mx,1:2)
      double precision theta
      double precision T
c     temp
      integer i,interval
      logical interval_found
c     label
      character*(Nchar_mx) label
      label='subroutine get_surface_temperature'

      interval_found=.false.
      do i=1,Nt-1
         if ((theta.ge.surf_T(i,1)).and.(theta.le.surf_T(i+1,1))) then
            interval_found=.true.
            interval=i
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (.not.interval_found) then
         call error(label)
         write(*,*) 'Could not find interval for theta=',theta,' deg'
         write(*,*) 'i / surf_T(i,1) [deg]'
         do i=1,Nt
            write(*,*) i,surf_T(i,1)
         enddo                  ! i
         stop
      endif
c     linear interpolation with latitude
      T=surf_T(interval,2)+(surf_T(interval+1,2)-surf_T(interval,2))*(theta-surf_T(interval,1))/(surf_T(interval+1,1)-surf_T(interval,1))

      return
      end
