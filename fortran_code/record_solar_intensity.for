      subroutine record_solar_intensity_file(Nlambda,intensity,solar_intensity_file)
      implicit none
      include 'max.inc'
      include 'size_params.inc'
      include 'param.inc'
c     
c     Purpose: to record a solar intensity binary file
c     
c     Input:
c       + Nlambda: number of wavelength/intensity values
c       + intensity: values of the wavelength [nm] and associated solar intensity [W/m²/sr/nm]
c       + solar_intensity_file: file to record
c     
c     Output: the required binary file
c     
c     I/O
      integer*8 Nlambda
      double precision intensity(1:Nlambda_sun_mx,1:2)
      character*(Nchar_mx) solar_intensity_file
c     temp
      integer*8 total_recorded
      integer remaining_byte
      logical*1 l1
      integer*8 record_size,alignment
      integer i,ilambda
c     label
      character*(Nchar_mx) label
      label='subroutine record_solar_intensity_file'

      record_size=int(2*size_of_double,kind(record_size))
      alignment=record_size
      
      open(23,file=trim(solar_intensity_file),form='unformatted',access='stream')
      write(23) int(pagesize,kind(Nlambda))
      write(23) int(Nlambda,kind(Nlambda))
      write(23) record_size
      write(23) alignment
c     Padding file 23 ---
      total_recorded=4*size_of_int8
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      do ilambda=1,Nlambda
         write(23) (intensity(ilambda,i),i=1,2) ! [nm], [W/m²/sr/nm]
      enddo                     ! j
c     Padding file 23 ---
      total_recorded=Nlambda*record_size
      call compute_padding2(pagesize,total_recorded,remaining_byte)
      write(23) (l1,i=1,remaining_byte)
c     --- Padding file 23
      close(23)
      write(*,*) 'File has been recorded: ',trim(solar_intensity_file)

      return
      end
