#!/bin/bash

camera_position_file='./camera_position.dat'
camera_target_file='./camera_target.dat'
camera_up_file='./camera_up.dat'

# Check if files exist
if [ ! -e "$camera_position_file" ]; then
    echo "$camera_position_file does not exist"
    exit 0
fi
if [ ! -e "$camera_target_file" ]; then
    echo "$camera_target_file does not exist"
    exit 0
fi
if [ ! -e "$camera_up_file" ]; then
    echo "$camera_up_file does not exist"
    exit 0
fi
# Reading camera positions
echo 'Reading camera positions'
iframe=0
while read -r line; do
    if [ $iframe == 0 ]; then
	Nframe=$line
    else
	x_camera[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 1)
	y_camera[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 2)
	z_camera[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 3)
	camera_position='pos='${x_camera[$iframe]}','${y_camera[$iframe]}','${z_camera[$iframe]}
	position_list[$iframe]=$camera_position
    fi
    iframe=$(($iframe+1))
done < "$camera_position_file"
Ncamera=${#x_camera[@]}
if [ $Ncamera -ne $Nframe ]; then
    echo 'Inconsistency: Ncamera='$Ncamera
    echo 'should be equal to Nframe=',$Nframe
    exit 1
fi
# Reading camera targets
echo 'Reading camera targets'
iframe=0
while read -r line; do
    if [ $iframe == 0 ]; then
	Nt=$line
    else
	x_target[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 1)
	y_target[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 2)
	z_target[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 3)
	target='tgt='${x_target[$iframe]}','${y_target[$iframe]}','${z_target[$iframe]}
	target_list[$iframe]=$target
    fi
    iframe=$(($iframe+1))
done < "$camera_target_file"
Ntarget=${#x_target[@]}
if [ $Ntarget -ne $Nt ]; then
    echo 'Inconsistency: Ntarget='$Ntarget
    echo 'should be equal to Nt=',$Nt
    exit 1
fi
# Reading camera ups
echo 'Reading camera ups'
iframe=0
while read -r line; do
    if [ $iframe == 0 ]; then
	Nup=$line
    else
	x_up[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 1)
	y_up[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 2)
	z_up[$iframe]=$(echo "$line" | tr -s " " | cut -d " " -f 3)
	up='up='${x_up[$iframe]}','${y_up[$iframe]}','${z_up[$iframe]}
	up_list[$iframe]=$up
    fi
    iframe=$(($iframe+1))
done < "$camera_up_file"
Nu=${#x_up[@]}
if [ $Nup -ne $Nu ]; then
    echo 'Inconsistency: Nup='$Nup
    echo 'should be equal to Nu=',$Nu
    exit 1
fi
# Some more checking
if [ $Nframe -ne $Nt ]; then
    echo 'Inconsistency: Nt=',$Nt
    echo 'should be equal to Nframe=',$Nframe
    exit 1
fi
if [ $Nframe -ne $Nu ]; then
    echo 'Inconsistency: Nu=',$Nu
    echo 'should be equal to Nframe=',$Nframe
    exit 1
fi
if [ $Nframe -ge 10000 ]; then
    echo 'Error: number of frames='$Nframe
    echo 'should be < 10000'
    exit 1
fi

export DATA_PATH="/home/eymet/Ext4To/Data/Rad-Net/random01"
if [ ! -d "$DATA_PATH" ]; then
    mkdir $DATA_PATH
fi
RAW_RESULTS='./RAW'
GAS="mesh=${DATA_PATH}/gas_grid.bin"
GAS="ck=${DATA_PATH}/gas_radiative_properties.bin:${GAS}"
GAS="temp=${DATA_PATH}/gas_thermodynamic_properties.bin:${GAS}"
GROUND="name=sphere"
GROUND="mesh=${DATA_PATH}/sphere.bin:${GROUND}"
GROUND="prop=${DATA_PATH}/surface_properties.bin:${GROUND}"
GROUND="brdf=${DATA_PATH}/materials_list.txt:${GROUND}"
SOURCE="lon=0:lat=0"
SOURCE="dst=1.495978707e8:${SOURCE}"
SOURCE="radius=6.96342e5:${SOURCE}"
SOURCE="temp=5773:${SOURCE}"
IMAGE_DEF="640x480"
IMAGE_SPP="128"
IMAGE="def=${IMAGE_DEF}:spp=${IMAGE_SPP}"
OCTREE_DEF="256"
OPTHICK="1"
STORAGE="octrees${OCTREE_DEF}_T${OPTHICK}.bin"
SPECTRAL="cie_xyz"

# loop over frames
for (( iframe=1 ; iframe<=$Nframe ; iframe++ )); 
do
    if [ $iframe -lt 10 ]; then
	INDEX='000'$iframe
    elif [ $iframe -lt 100 ]; then
	INDEX='00'$iframe
    elif [ $iframe -lt 1000 ]; then
	INDEX='0'$iframe
    elif [ $iframe -lt 10000 ]; then
	INDEX=$iframe
    else
	echo 'Error: number of frames='$Nframe
	echo 'should be < 10000'
	exit 1
    fi
    CAMERA="${position_list[$iframe]}"
    CAMERA="${target_list[$iframe]}:${CAMERA}"
    CAMERA="${up_list[$iframe]}:${CAMERA}"
    CAMERA="fov=60:${CAMERA}"
    OUTPUT="${RAW_RESULTS}/random01_${IMAGE_DEF}x${IMAGE_SPP}_${INDEX}.txt"
    if [ -e $OUTPUT ]; then
	echo 'File already exists: '$OUTPUT
    else
	echo '------------------------------------------------------------'
	echo 'Rendering of image '$iframe' / '$Nframe
	echo '------------------------------------------------------------'
	# Invocation of htrdr-planeto
	htrdr-planeto -v -N\
		      -G "${GROUND}"\
		      -g "${GAS}"\
		      -S "${SOURCE}"\
		      -s "${SPECTRAL}"\
		      -C "${CAMERA}"\
		      -i "${IMAGE}"\
		      -V "${OCTREE_DEF}" -T "${OPTHICK}" -O "${STORAGE}"\
		      -fo "${OUTPUT}"
    fi
done

exit 0
