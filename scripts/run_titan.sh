#!/bin/sh -e

export DATA_PATH="/home/eymet/Ext4To/Data/Rad-Net/Titan"

GAS="mesh=${DATA_PATH}/gas_grid.bin"
GAS="${GAS}:ck=${DATA_PATH}/gas_radiative_properties.bin"
GAS="${GAS}:temp=${DATA_PATH}/gas_thermodynamic_properties.bin"

HAZE="name=haze"
HAZE="${HAZE}:mesh=${DATA_PATH}/haze_grid.bin"
HAZE="${HAZE}:radprop=${DATA_PATH}/haze_radiative_properties.bin"
HAZE="${HAZE}:phasefn=${DATA_PATH}/haze_phase_function_list.txt"
HAZE="${HAZE}:phaseids=${DATA_PATH}/haze_phase.bin"

CLOUD="name=cloud"
CLOUD="${CLOUD}:mesh=${DATA_PATH}/cloud_grid.bin"
CLOUD="${CLOUD}:radprop=${DATA_PATH}/cloud_radiative_properties.bin"
CLOUD="${CLOUD}:phasefn=${DATA_PATH}/cloud_phase_function_list.txt"
CLOUD="${CLOUD}:phaseids=${DATA_PATH}/cloud_phase.bin"

GROUND="name=titan_surface"
GROUND="${GROUND}:mesh=${DATA_PATH}/sphere.bin"
GROUND="${GROUND}:prop=${DATA_PATH}/surface_properties.bin"
GROUND="${GROUND}:brdf=${DATA_PATH}/materials_list.txt"

SOURCE="lon=180:lat=0"
SOURCE="${SOURCE}:dst=3233089.0"
SOURCE="${SOURCE}:radius=6.96342e5"
SOURCE="${SOURCE}:temp=5773"

CAMERA="pos=12000000,0.0,0.0"
CAMERA="${CAMERA}:tgt=0.0,0.0,0.0"
CAMERA="${CAMERA}:up=0.0,0.0,1.0"
CAMERA="${CAMERA}:fov=30"

IMAGE_DEF="640x480"
IMAGE_SPP="16"
IMAGE="def=${IMAGE_DEF}:spp=${IMAGE_SPP}"

OCTREE_DEF="256"
OPTHICK="1"
STORAGE="octrees${OCTREE_DEF}_T${OPTHICK}.bin"
SPECTRAL="cie_xyz"
OUTPUT="titan_${IMAGE_DEF}x${IMAGE_SPP}.txt"

htrdr-planeto -v -N \
 -G "${GROUND}" \
 -g "${GAS}" \
 -a "${HAZE}" \
 -a "${CLOUD}" \
 -S "${SOURCE}" \
 -s "${SPECTRAL}" \
 -C "${CAMERA}" \
 -i "${IMAGE}" \
 -V "${OCTREE_DEF}" -T "${OPTHICK}" -O "${STORAGE}" \
 -fo "${OUTPUT}"
